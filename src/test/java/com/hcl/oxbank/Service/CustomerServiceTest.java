package com.hcl.oxbank.Service;

import com.hcl.oxbank.dto.CustomerDto;
import com.hcl.oxbank.entity.Customer;
import com.hcl.oxbank.repository.CustomerRepository;
import com.hcl.oxbank.service.CustomerService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Optional;

@ExtendWith({MockitoExtension.class})
public class CustomerServiceTest {

    @Mock
    private CustomerRepository customerRepository;

    @InjectMocks
    private CustomerService customerService;

    @Test
    public void testDatasave(){
        CustomerDto customerDto=new CustomerDto();
        customerDto.setName("abcde");
        customerDto.setMobile("8073843043");
        customerDto.setDob("01/02/1998");
        customerDto.setGender("female");
        customerDto.setMaritalstatus("single");
        customerDto.setCreditscore(300);
        customerDto.setSalary(40000);
        customerDto.setExpense(4000);

        Customer customer=new Customer();
        customer.setName(customerDto.getName());
        customer.setMobile(customerDto.getMobile());
        customer.setDob(customerDto.getDob());
        customer.setGender(customerDto.getGender());
        customer.setMaritalstatus(customerDto.getMaritalstatus());
        customer.setCreditscore(customerDto.getCreditscore());
        customer.setSalary(customerDto.getSalary());
        customer.setExpense(customerDto.getExpense());

        Mockito.when(customerRepository.save(customer)).thenReturn(customer);
        Customer result=customerService.datasave(customerDto);
        Assertions.assertEquals(customer,result);
    }

    @Test
    public void testGetDataByID(){
        int customerID=1;
        Customer expectedcustomer=new Customer();
        expectedcustomer.setID(customerID);
        Mockito.when(customerRepository.findById(customerID)).thenReturn(Optional.of(expectedcustomer));
        Customer result=customerService.getDataByID(customerID);
        Assertions.assertEquals(expectedcustomer,result);
    }
}